#include <stdio.h>
#include <stdlib.h>

typedef struct nodo{
    int valor;
    struct nodo *siguienteNodo;
}Elemento;

Elemento *crearListaEnlazada(Elemento *lista){
    lista = NULL;
    return lista;
}

Elemento *agregarElemento(Elemento *lista,int valor){
    Elemento *nuevoElemento;
    nuevoElemento = malloc(sizeof(Elemento));
    nuevoElemento->valor = valor;
    nuevoElemento->siguienteNodo = NULL;
    if (lista == NULL){
        lista = nuevoElemento;
    }else{
        Elemento *aux = lista;
        while (aux->siguienteNodo !=NULL){
            aux = aux->siguienteNodo;
        }
        aux->siguienteNodo = nuevoElemento;
    }
    return lista;
}
void imprimirLista(Elemento *lista){
    while (lista != NULL){
        printf("%d\n",lista->valor);
        lista = lista->siguienteNodo;
    }
    
}
void obtenerLonguitudDeLaLista(Elemento *lista){
    int contador = 0;
    while (lista != NULL){
        contador ++;
        lista = lista->siguienteNodo;
    }
    printf("La cantidad de elementos en la lista es : %d\n", contador);
}
void obtenerElementoDeLaLista(Elemento *lista, int valor){
    int contador = 1;
    int hayElemento = 0;
    while(lista !=NULL){
        if(contador == valor ){
            hayElemento=1;
            printf("El valor en la posicion %d es: %d\n",valor,lista->valor);
            
        }
        lista = lista->siguienteNodo;
        contador ++;
    }
    if (hayElemento != 1){
        printf("No hay un elemento en la posicion %d\n",valor);
    }
    
    
}

void eliminarElementoDeLaLista(Elemento *lista, int valor){
    if (valor < 0 || valor >= obtenerLonguitudDeLaLista(lista)) {
        printf("Error: índice fuera de rango\n");
    }
    Elemento *actual = lista;
    if (valor == 0) {
        lista = actual->siguienteNodo;
        free(actual);
    } else {
        int i = 0;
        while (i < valor - 1) {
            actual = actual->siguienteNodo;
            i++;
        }
        Elemento *aux = actual->siguienteNodo;
        actual->siguienteNodo = aux->siguienteNodo;
        free(aux);
    }
}

/*------------------------------------------PARTE 1---------------------------------------------------*/

Elemento  *agregarElementoOrdenado(Elemento *lista, int valor ){
    Elemento *nuevoElemento;
    nuevoElemento = malloc(sizeof(Elemento));
    nuevoElemento->valor = valor;
    nuevoElemento->siguienteNodo = NULL;
    Elemento  *elemento = lista;
    if( lista == NULL){
        lista = nuevoElemento;
    }else if(lista->valor >= valor){
        nuevoElemento->siguienteNodo = lista;
        lista = nuevoElemento;
    }else{
    while(elemento->siguienteNodo!= NULL && elemento->siguienteNodo->valor < valor){
        elemento = elemento->siguienteNodo;
    }
    nuevoElemento->siguienteNodo = elemento->siguienteNodo;
    elemento->siguienteNodo = nuevoElemento;
    }
    return lista;
}

/*------------------------------------------PARTE2-----------------------------------------------------*/

int main(){
    Elemento *lista = crearListaEnlazada(lista);
    lista = agregarElementoOrdenado(lista,10);
    lista = agregarElementoOrdenado(lista,5);
    lista = agregarElementoOrdenado(lista,3);
    lista = agregarElementoOrdenado(lista,2);
    imprimirLista(lista);
    lista = eliminarElementoDeLaLista(lista,2);
    imprimirLista(lista);
    obtenerLonguitudDeLaLista(lista);
    obtenerElementoDeLaLista(lista, 3);
    return 0;
}
