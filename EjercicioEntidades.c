/* Como ejercicio: crear una entidad persona con todos los atributos 
necesarios para definir esta entidad de negocio (a elección). 
Crear funciones que permitan crear la entidad, modificar sus datos, 
imprimir la información de la entidad. Interpretar qué sucede a nivel
memoria cuando llamamos a una función y le pasamos el struct */

#include <stdio.h>
#include <string.h>


typedef struct{
    char nombre[50];
    char apellido[50];
    int edad;
    int dni;
}Usuario;

Usuario ingresarUsuario(char nombre[],char apellido[],int dni,int edad){
    Usuario instancia;
    strcpy(instancia.nombre, nombre);
    strcpy(instancia.apellido, apellido);
    instancia.dni = dni;
    instancia.edad = edad;
    return instancia;
}

void modificarDatos(Usuario *instancia,char nombre[],char apellido[],int dni,int edad){
    strcpy(instancia->nombre, nombre);
    strcpy(instancia->apellido,apellido);
    instancia->dni = dni;
    instancia->edad = edad;

}
void mostrarInformacion(Usuario instancia){
    printf("Datos del usuario \n");
    printf("El nombre es: %s\n", instancia.nombre);
    printf("El apellido es: %s\n", instancia.apellido);
    printf("La edad es: %d\n", instancia.edad);
    printf("El dni es: %d\n", instancia.dni);
    

};

int main() {
    int numero;
    Usuario instancia;
    do{
        printf(" \n ITAU \n \n Bienvenido a home banking ;) \n 1.Iniciar sesion \n 2.Ver datos personales \n 3.Modificar datos personales\n 4.Salir \n");
        printf("\n Ingresar opcion: \n");
        scanf("%d", &numero);
        switch(numero){
            case 1:
                printf("Ingrese nombre:\n");
                scanf("%s",&instancia.nombre);
                printf("Ingrese apellido:\n");
                scanf("%s",&instancia.apellido);
                printf("Ingrese edad:\n");
                scanf("%d",&instancia.edad);
                printf("Ingrese dni:\n");
                scanf("%d",&instancia.dni);
                printf("\n Sesion iniciada \n");
                ingresarUsuario(instancia.nombre,instancia.apellido,instancia.dni,instancia.edad);
                break;
            case 2:
                mostrarInformacion(instancia);
                break;
            case 3:
                printf("Para modificar sus datos debera ingresar los mismos nuevamente incluyendo los que no desee modificar \n");
                
                printf("Ingrese nombre:\n");
                scanf("%s",&instancia.nombre);
                printf("Ingrese apellido:\n");
                scanf("%s",&instancia.apellido);
                printf("Ingrese edad:\n");
                scanf("%d",&instancia.edad);
                printf("Ingrese dni:\n");
                scanf("%d",&instancia.dni);
                modificarDatos(&instancia,instancia.nombre,instancia.apellido,instancia.dni,instancia.edad);
                break;
            case 4:
                printf(" \n Cerrando sesion... \n");
                break;
            default:
                printf(" \n Error al ingresar opcion: La opcion que desea ingresar no existe \n");
        }
    } while(numero!=4);
    return 0;
}
