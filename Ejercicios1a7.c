#include <stdio.h>
/* EJERCICIO 1 */
int main() {
    char nombre[100];
    int edad;
    printf("Ingrese su nombre y edad\n");
    scanf("%s", nombre);
    scanf("%d", &edad);
    printf("Su nombre es : %s \n", nombre);
    printf("Su edad es: %d \n", edad);
    return 0;
}

/* EJERCICIO 2 */
#include <stdio.h>

int main() {
    int numeros[] = {1,2,3,4,5};
    int maximo = numeros[0];
    for(int i=1; i < sizeof(numeros) / sizeof(int); i++){
        if(numeros[i] > maximo){
            maximo = numeros[i];
        }
    }
    printf("el maximo numero es: %d \n", maximo);
    return 0;
}

/* EJERCICIO 3 */
#include <stdio.h>

int main() {
    int numeros[] = {1,2,3,4,5};
    int minimo = numeros[0];
    for(int i=1; i < sizeof(numeros) / sizeof(int); i++){
        if(numeros[i] < minimo){
            minimo = numeros[i];
        }
    }
    printf("el minimo numero es: %d \n", minimo);
    return 0;
}

/* EJERCICIO 4 1er forma de resolucion */
#include <stdio.h>

int main() {
    int numero1;
    int numero2;
    int numero3;
    int promedio = 0;
    printf("Ingrese tres numeros enteros:\n");
    scanf("%d %d %d", &numero1, &numero2, &numero3);
    promedio = (numero1+numero2+numero3)/3;
    printf("El promedio de los tres numeros es: %d \n", promedio);
    return 0;
}

/* EJERCICIO 4 2da forma de resolucion */
#include <stdio.h>

int main() {
    int numeros[3];
    int promedio = 0;
    int sumatoria = 0;
    printf("Ingrese tres numeros enteros:\n");
    scanf("%d %d %d", &numeros[0], &numeros[1], &numeros[2]);
    for(int i=0;i < sizeof(numeros) / sizeof(int); i++){
        sumatoria= sumatoria+ numeros[i];
        promedio = (sumatoria)/sizeof(int)+1;
    }
    printf("El promedio de los tres numeros es: %d \n", promedio);
    return 0;
}

/* EJERCICIO 5 */
#include <stdio.h>

int main() {
    for(int i = 0; i < 128; i++) {
        printf("%c \n", i);
    }
    return 0;
}

/* EJERCICIO 6 */
#include <stdio.h>

int main() {
    int numero;
    printf("Ingrese un numero:\n");
    scanf("%d", &numero);
    if ((numero%2)==0){
        printf("Su numero es par \n");
    }
    else {
        printf("Su numero no es par \n");
    } 
    return 0;
}

/* EJERCICIO 7 */
#include <stdio.h>

int main() {
    int numero;
    do{
        printf(" \n MENU \n 1.Opcion.1 \n 2.Opcion.2 \n 3.Opcion.3 \n 4.Salir \n");
        printf("\n Ingrese opcion: \n");
        scanf("%d", &numero);
        switch(numero){
            case 1:
                printf(" \n Chequeando valvula escape.... Valvula funcionando correctamente \n");
                break;
            case 2:
                printf(" \n Midiendo niveles de contaminacion.... Nivel de contaminacion al 10 por ciento \n");
                break;
            case 3:
                printf(" \n Corriendo diagnostico del equipo... El equipo se encuentra en excelentes condiciones \n");
                break;
            case 4:
                printf(" \n Cerrando sistema... \n");
                break;
            default:
                printf(" \n Error al ingresar opcion \n");
        }
    } while(numero!=4);
    return 0;
}
